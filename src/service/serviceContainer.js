import bannerData from '@/assets/temp/banner.json'
import servicesData from '@/assets/temp/services.json'
import tourProgramFirst from '@/assets/temp/tour_program_first.json'
import answers from '@/assets/temp/answers.json'
import travelDetails from '@/assets/temp/travel_details.json'
import bron from '@/assets/temp/bron.json'
import footer from '@/assets/temp/footer.json'
import together from '@/assets/temp/together.json'
import { animOnScroll } from './animation.js'
import service from './service'

export default {
  together,
  bannerData,
  servicesData,
  tourProgramFirst,
  answers,
  travelDetails,
  bron,
  footer,
  animOnScroll,
  service
}